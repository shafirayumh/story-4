from django.apps import AppConfig


class ProfilewebsiteConfig(AppConfig):
    name = 'profilewebsite'
