from django import forms
from .models import Schedule
from django.forms import ModelForm

class ScheduleForm(forms.Form):
    date_time = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'])
    date_end = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'])
    category = forms.CharField(max_length=50)
    activity = forms.CharField(max_length=50)
    location = forms.CharField(max_length=50)