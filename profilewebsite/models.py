from django.db import models

# Create your models here.

class Schedule(models.Model):
    date_time = models.DateTimeField('date_time')
    date_end = models.DateTimeField('date_end')
    category = models.CharField(max_length=50, default = "")
    activity = models.CharField(max_length=50, default = "")
    location = models.CharField(max_length=50, default = "")
