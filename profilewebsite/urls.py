from django.urls import path
from . import views


app_name = 'story4'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('experience/', views.experience, name='experience'),
    path('abilities/', views.abilities, name='abilities'),
    path('contact/', views.contact, name='contact'),
    path('schedule/', views.schedule, name='schedule'),
    path('view_schedule/', views.view_schedule, name='view_schedule'),
    path('add_schedule/', views.add_schedule, name='add_schedule'),
    path('delete/', views.delete, name="delete"),   
]

