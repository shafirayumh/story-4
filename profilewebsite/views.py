from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.shortcuts import redirect
import datetime


# Create your views here.
def home(request):
    return render(request, 'home.html')

def profile(request):
    return render(request, 'profile.html')

def abilities(request):
    return render(request, 'abilities.html')

def experience(request):
    return render(request, 'experience.html')

def contact(request):
    return render(request, 'contact.html')

def schedule(request):
    list_sched = Schedule.objects.all().values()
    return render(request, 'sched_form.html', {'sched' : list_sched})

def add_schedule(request):
    if request.method == 'POST' :
        print("hai")
        form = ScheduleForm(request.POST)
        if form.is_valid():
            date_time = form.data['date_time']
            date_end = form.data['date_end']
            category = form.data['category']
            activity = form.data['activity']
            location = form.data['location']
            sched = Schedule(activity=activity,date_time=date_time, date_end=date_end, category=category, location=location)
            sched.save()
    return redirect('/view_schedule/')

def view_schedule(request):
    list_sched = Schedule.objects.all().values()
    return render(request, 'sched_edit.html', {'sched' : list_sched})

def delete(request):
	if request.method == 'POST'  :
		id = request.POST['id']
		Schedule.objects.filter(id=id).delete()
	return redirect('/view_schedule')
